<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Message queue service</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <script src="https://unpkg.com/vue@3"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!--    <script src="https://cdn.bootcdn.net/ajax/libs/vuex/4.0.2/vuex.global.js">-->

    <script>
        const API_URL = 'http://symfony-site/test-articles/api/public';
    </script>
    <style>

        .top-menu-active {
            border-bottom: 2px white solid;
        }

        .tab-field-box {
            border-bottom: 1px gainsboro solid;
            width: 80px;
        }

        .edit-form-item-wave-box {
            border: 1px gainsboro solid;
            padding: 8px 8px 0px 8px;
            border-radius: 3px;
        }

        .btn-small-size {
            border-radius: 3px;
            padding: 0px 8px 0px 8px !important;
            font-size: 11px !important;
            height: 40px;
            text-align: center !important;
        }

        .paginator {
            display: flex;
            padding: 5px;
        }

        .paginator-item {
            width: 40px;
            border: 1px lightskyblue solid;
            text-align: center;
        }

        .paginator-item-active {
            width: 40px;
            border: 1px lightskyblue solid;
            text-align: center;
            background: cornflowerblue;
            color: white;
            border-radius: 4px;
        }

        .paginator-item:hover {
            background: aliceblue;
            cursor: pointer;
            border-radius: 4px;
        }

    </style>

</head>
<body>
<div id="vue-app" class="wrapper">

    <div class="bg-white">

        <header class="relative bg-white" style="margin-bottom: 20px">
            <div class="bg-gray-800 h-15 text-sm font-medium text-white" style="">
                <nav class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 text-white">
                    <div class="border-b border-gray-800">
                        <div class="h-16 flex items-center">
                            <button type="button" class="bg-white p-2 rounded-md text-gray-400 lg:hidden">
                                <span class="sr-only">Open menu</span>
                                <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="2" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M4 6h16M4 12h16M4 18h16"/>
                                </svg>
                            </button>

                            <div class="ml-4 flex lg:ml-0">
                                <a href="#">
                                    <span class="sr-only">Workflow</span>
                                    <img class="h-8 w-auto"
                                         src="https://tailwindui.com/img/logos/workflow-mark.svg?color=indigo&shade=600"
                                         alt="">
                                </a>
                            </div>

                            <div class="hidden lg:ml-8 lg:block lg:self-stretch">
                                <div class="h-full flex space-x-8">
                                    <a href="#articles" @click="loadPage('#articles', $event)"
                                       class="flex items-center text-sm font-medium hover:text-gray-400 top-menu-list"
                                       style="">Статьи</a>
                                    <a href="#rating" @click="loadPage('#rating', $event)"
                                       class="flex items-center text-sm font-medium hover:text-gray-400 top-menu-list">Рейтиг</a>
                                </div>
                            </div>
                            <div class="ml-auto flex items-center">

                                <div class="hidden lg:flex lg:flex-1 lg:items-center lg:justify-end lg:space-x-6">
                                    <a @click="userFormInit('login')" href="#"
                                       class="text-sm font-medium hover:text-gray-400">Войти</a>
                                    <span class="h-6 w-px bg-gray-200" aria-hidden="true"></span>
                                    <a @click="userFormInit('create')" href="#"
                                       class="text-sm font-medium hover:text-gray-400">Регистрация</a>
                                </div>


                                <div class="flex lg:ml-6">
                                    <a href="#" class="p-2 text-gray-400 hover:text-gray-500">
                                        <span class="sr-only">Search</span>
                                        <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none"
                                             viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                                             aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                  d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
                                        </svg>
                                    </a>
                                </div>


                                <div class="ml-4 flow-root lg:ml-6">
                                    <a href="#" class="group -m-2 p-2 flex items-center">

                                        <svg class="flex-shrink-0 h-6 w-6 text-gray-400 group-hover:text-gray-500"
                                             xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                             stroke="currentColor" stroke-width="2">
                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                  d="M8 10h.01M12 10h.01M16 10h.01M9 16H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-5l-5 5v-5z"/>
                                        </svg>

                                        <span class="ml-2 text-sm font-medium text-gray-700 group-hover:text-gray-800">0</span>
                                        <span class="sr-only">items in cart, view bag</span>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </header>

        <user-form
                v-if="userFormOpen"
                :action_type="getUserFormActionType"
                @action_user="userFormResponse"
                @modal_close="userModalClose"
        />

    </div>

    <div class="bg-white">

        <main class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <section aria-labelledby="products-heading" class="pt-6 pb-24">

                <div style="display: flex; margin: -20px 0px 20px 0px; border-bottom: 2px gainsboro solid; padding: 0px 0px 10px 0px">

                    <div style="width: 50%">
                        Категория: {{categoryTitle}} / Всего записей: {{getTotalCount}} / На странице: {{limit}} /
                        Страница: {{page}} / Пользователь: {{userName}}
                    </div>

                    <div style="width: 40%; margin-left: auto">

                        <div style="display: flex; width: 100%">
                            <button @click="formOpenToggle()" style="margin-left: auto"
                                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                            > Добавить статью
                            </button>
                        </div>

                        <item-form v-if="formOpen"
                                   style="position: absolute; width:450px; margin-right: 0px; margin-top: 10px;"
                                   :author="userName"
                                   @add_article="addArticleResponse"/>

                    </div>

                </div>


                <div class="grid grid-cols-1 lg:grid-cols-4 gap-x-8 gap-y-10">

                    <!-- ЛЕВАЯ ПАНЕЛЬ -->
                    <div class="hidden lg:block">
                        <ul role="list" class="space-y-9">
                            <li><h2 class="font-display font-medium text-slate-900 dark:text-white">Категории</h2>
                                <ul role="list"
                                    class="mt-2 space-y-2 border-l-2 border-slate-100 dark:border-slate-800 lg:mt-4 lg:space-y-4 lg:border-slate-200">
                                    <li class="relative">
                                        <a @click="selectCategory({ id: 0, title: 'Все категории'}, 0)"
                                           :class="elemClass"
                                           href="#">Все категории</a>
                                    </li>
                                    <template v-for="(item, index) in categories">
                                        <li v-if="getCategoryActive == item.id" class="relative">
                                            <a @click="selectCategory(item, index)" :class="elemActiveClass"
                                               href="#">{{item.title}}</a>
                                        </li>
                                        <li v-else class="relative">
                                            <a @click="selectCategory(item, index)" :class="elemClass"
                                               href="#">{{item.title}}</a>
                                        </li>
                                    </template>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <!-- ПРАВАЯ ПАНЕЛЬ -->
                    <div class="lg:col-span-3">

                        <div style="position: absolute; z-index: 99999; background: aliceblue; padding: 10px; top:0px; right: 4px;">
                            <user-likes v-if="usLikesPanelToggle" style="width: 400px"/>
                        </div>

                        <div class="relative z-10 bg-white rounded-xl shadow-xl ring-1 ring-slate-900/5 divide-y divide-slate-100 my-auto xl:mt-18 dark:bg-slate-800 dark:divide-slate-200/5 dark:highlight-white/10">

                            <nav class="py-4 px-4 sm:px-6 lg:px-4 xl:px-6 text-sm font-medium">
                                <ul class="flex space-x-1">
                                    <li><div @click="usLikesPanelToggle = !usLikesPanelToggle" class="px-3 py-2 rounded-md bg-slate-50 cursor-pointer dark:bg-transparent dark:text-slate-300
                                                    dark:ring-1 dark:ring-slate-700 bg-indigo-50 hover:text-indigo-700 hover:bg-indigo-200">
                                            Список оценивших пользователей
                                    </div></li>
                                </ul>
                            </nav>

                            <article
                                    v-for="(item) in articles"
                                    class="p-4 sm:p-6 lg:p-4 xl:p-6 space-x-4 items-start sm:space-x-6 lg:space-x-4 xl:space-x-6 flex">
                                <div class="min-w-0 relative flex-auto">
                                    <h2 class="font-semibold text-slate-900 truncate sm:pr-20 dark:text-slate-100">
                                        {{item.title}}</h2>
                                    <dl class="mt-2 flex flex-wrap text-sm leading-6 font-medium">

                                        <div class="hidden absolute top-0 right-0 sm:flex items-center space-x-1 dark:text-slate-100">
                                            <dt @click="deleteArticle(item)" class="text-sky-500"
                                                style="cursor: pointer">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none"
                                                     viewBox="0 0 24 24" stroke="red" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                          d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                                </svg>
                                            </dt>
                                        </div>

                                        <div class="dark:text-slate-200" style="font-style: italic; font-size: 10px;">
                                            <dd class="px-1.5 ring-1 ring-slate-200 rounded dark:ring-slate-300">
                                                Категория: {{categories[item.category_id].title}}
                                            </dd>
                                        </div>
                                        <div>
                                            <dd class="flex items-center"
                                                style="font-style: italic; font-size: 10px; margin-left: 20px">
                                                Автор: {{item.author}}
                                            </dd>
                                        </div>
                                        <div>
                                            <dd @click="addLike(item)" class="flex items-center"
                                                style="font-style: italic; font-size: 10px; margin-left: 20px; cursor: pointer">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5"
                                                     viewBox="0 0 20 20" fill="blue">
                                                    <path fill-rule="evenodd"
                                                          d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                                                          clip-rule="evenodd"/>
                                                </svg>
                                            </dd>
                                        </div>
                                        <div>
                                            <dd class="flex items-center"
                                                style="font-style: italic; font-size: 10px; margin-left: 20px">
                                                Likes: {{item.likes_count}}
                                            </dd>
                                        </div>
                                        <div class="flex-none w-full mt-2 font-normal">
                                            <dt class="sr-only">Cast</dt>
                                            <dd class="text-slate-400">{{item.content}}</dd>
                                        </div>
                                        <div class="flex-none w-full mt-2 font-normal">
                                            <dt class="sr-only">Cast</dt>
                                            <dd class="text-slate-300" style="font-style: italic; font-size: 10px">
                                                {{item.publish_at}}
                                            </dd>
                                        </div>
                                    </dl>
                                </div>
                            </article>

                        </div>

                        <div class="paginator">
                            <template v-for="(pageNum) in getPaginatorList">
                                <div v-if="page == pageNum" @click="loadPaginatorPage(pageNum)"
                                     class="paginator-item-active">{{pageNum}}
                                </div>
                                <div v-else @click="loadPaginatorPage(pageNum)" class="paginator-item">{{pageNum}}</div>
                            </template>
                        </div>

                    </div>

                </div>

            </section>
        </main>

    </div>

</div>
</body>

<script src="resource/components.js" type="application/javascript"></script>
<script src="resource/app.js" type="application/javascript"></script>

</html>
