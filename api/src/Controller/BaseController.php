<?php

namespace App\Controller;

use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends AbstractController
{
    protected $jwtStatus = false;
    protected $userId    = 0;
    protected $userRole  = 0;
    protected $request;

    public function __construct() {
        $this->request = Request::createFromGlobals();
    }

    protected function getSecretKey() {
        return 'gfhdgYU546mbnhBN';
    }

    protected function userJwtVerify() {

        $token = $this->request->headers->get('user-jwt-token');
        if(!$token)
            return false;

        $secretKey = $this->getSecretKey();

        try {
            $data = JWT::decode($token, $secretKey, ['HS256']);
            if(!empty($data->user_id)) {
                $this->userId    = $data->user_id;
                $this->userRole  = $data->role;
                $this->jwtStatus = true;
            }

        } catch (\Exception $e) {
            dump($e->getMessage());
        }

        return $this->jwtStatus;
    }

    public function findItem($class, $id, $format = 'array') {
        $repo = $this->getDoctrine()->getRepository($class);
        $record = $repo->find($id);
        if(empty($record)) return [];
        $result = $this->formattedData($repo, [$record], $format);
        return $result[0];
    }

    protected function findByItems($class, $conditions = [], $format = 'array')
    {
        $repo = $this->getDoctrine()->getRepository($class);
        if(!empty($conditions)) {
            $records = $repo->findBy($conditions);
        } else {
            $records = $repo->findAll();
        }
        if(empty($records)) return [];
        $results = $this->formattedData($repo, $records, $format);
        return $results;
    }

    public function formattedData($repo, $data, $format = 'array') {
        switch ($format) {
            case 'array' :
                $results = $repo->formatToArray($data);
                break;
            default :
                $results = $data;
                break;
        }
        return $results;
    }

    public function getRepo($class) {
        return $this->getDoctrine()->getRepository($class);
    }

    public function getRawData() {
        $data = file_get_contents("php://input");
        $data = json_decode($data);
        return $data;
    }

    protected function transformJsonBody(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if ($data === null) return $request;
        $request->request->replace($data);
        return $request;
    }
}