<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\ArticleLikes;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article/list/{cat_id}/{page}/{limit}", name="article_list")
     */
    public function getList($cat_id = 0, $page = 0, $limit = 0): JsonResponse
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);
        $articles = $repo->getArticles($cat_id, $page, $limit);
        $totalCount = $repo->getArticlesFeedCount($cat_id);
        $data = $this->dataConvert($articles);
        return $this->json(['result' => $data, 'total_count' => $totalCount]);
    }

    /**
     * @Route("/article/create", name="create_article")
     */
    public function create(Request $request): JsonResponse
    {

        $json = json_decode($request->getContent());
        $title = $json->title;
        $author = $json->author;
        $content = $json->content;
        $categoryId = $json->category_id;
        $date = date("Y-m-d H:i:s");

        $article = new Article();
        $article->setTitle($title);
        $article->setContent($content);
        $article->setAuthor($author);
        $article->setPublishedAt($date);
        $article->setCategory($categoryId);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($article);
        $entityManager->flush();

        return $this->json([
            'message' => 'Новая статья создана! (' .$article->getId(). ')',
            'save'  => 'ok',
        ]);
    }

    /**
     * @Route("/article/update/{id}", name="update_arcticle", methods={"POST"})
     */
    public function update($id, Request $request): JsonResponse
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);
        $article = $repo->findOneBy(['id' => $id]);

        $json = json_decode($request->getContent());
        $title = $json->title;
        $author = $json->author;
        $content = $json->content;
        $categoryId = $json->category_id;

        $article->setTitle($title);
        $article->setContent($content);
        $article->setAuthor($author);
        $article->setCategory($categoryId);

        return $this->json([
            'message' => 'Статья успешно изменена!',
            'save'  => 'ok',
        ]);
    }

    /**
     * @Route("/article/delete/{id}", name="delete_article", methods={"GET"})
     */
    public function delete($id): JsonResponse
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);
        $article = $repo->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($article);
        $entityManager->flush();

        return $this->json([
            'message' => 'Статья удалена!',
            'save'  => 'ok',
        ]);
    }

    /**
     * @Route("/article/get-one/{id}", name="get_one_article", methods={"GET"})
     */
    public function getOne($id): JsonResponse
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);
        $article = $repo->findOneBy(['id' => $id]);
        $data = $this->dataConvert([$article]);
        return $this->json(['result' => $data[0]]);
    }

    protected function dataConvert($articles) {
        $data = [];
        $likeRepo = $this->getDoctrine()->getRepository(ArticleLikes::class);
        foreach ($articles as $item) {
            $articleId = $item->getId();
            $likes = $likeRepo->getArticleLikesCount($articleId);
            $article = [
                'id'          => $item->getId(),
                'title'       => $item->getTitle(),
                'content'     => $item->getContent(),
                'category_id' => $item->getCategory(),
                'publish_at'  => $item->getPublishedAt(),
                'author'      => $item->getAuthor(),
                'likes_count' => $likes,
            ];
            $data[] = $article;
        }

        return $data;
    }

}
