<?php

namespace App\Controller;

use App\Entity\ArticleLikes;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleLikesController extends AbstractController
{
    /**
     * @Route("/article/likes/all", name="article_likes_all")
     */
    public function index(): JsonResponse
    {
        $likes = $this->getDoctrine()->getRepository(ArticleLikes::class)->findAll();
        $results = dataConvert($likes);
        return $this->json([
            'result' => $results,
        ]);
    }

    /**
     * @Route("/article/likes/{id}/{type}", name="article_likes_by")
     */
    public function getList($id, $type = 'article_id'): JsonResponse
    {
        $repo   = $this->getDoctrine()->getRepository(ArticleLikes::class);
        $records = $repo->getLikes($id, $type);
        $data    = $this->dataConvert($records);
        return $this->json(['result' => $data]);
    }


    /**
     * @Route("/article/active-users/likes", name="active-users")
     */
    public function getActiveLikeUsers(): JsonResponse
    {
        $repo    = $this->getDoctrine()->getRepository(ArticleLikes::class);
        $records = $repo->getActiveLikeUsers();
        $data = [];
        foreach ($records as $item) {
            $userId = $item['user_id'];
            $data[$userId][] = $item;
        }
        return $this->json(['result' => $data]);
    }

    /**
     * @Route("/article/like/create", name="create_article_like")
     */
    public function create(Request $request): JsonResponse
    {

        $json = json_decode($request->getContent());
        $userId = $json->user_id;
        $articleId = $json->article_id;
        $date = date("Y-m-d H:i:s");

        $repo  = $this->getDoctrine()->getRepository(ArticleLikes::class);
        $like  = $repo->getLikeCompare($userId, $articleId);
        if(!empty($like)) {
            return $this->json([
                'message' => 'Вы уже ставили лайк на эту статью',
                'save'  => 'ok',
            ]);
        }

        $model = new ArticleLikes();
        $model->setUserId($userId);
        $model->setArticleId($articleId);
        $model->setCreatedAt($date);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($model);
        $entityManager->flush();

        return $this->json([
            'message' => 'Новый лайк! (' .$model->getId(). ')',
            'save'  => 'ok',
        ]);
    }

    protected function dataConvert($items) {
        $data = [];
        foreach ($items as $item) {
            $data[] = [
                'id'         => $item->getId(),
                'user_id'    => $item->getUserId(),
                'article_id' => $item->getArticleId(),
                'created_at' => $item->getCreatedAt(),
            ];
        }
        return $data;
    }
}
