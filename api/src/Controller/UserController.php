<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Firebase\JWT\JWT;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use App\Entity\User;

class UserController extends AbstractController
{
    /**
     * @Route("/user/list", name="user_list")
     */
    public function index(): Response
    {
        $list = $this->getDoctrine()->getRepository(User::class)->findAll();
        $users = $this->formatToArray($list);
        return $this->json(['users' => $users]);
    }

    /**
     * @Route("/user/login", name="user_login")
     */
    public function login(Request $request) : Response
    {

        $json = json_decode($request->getContent());
        $email = $json->email;
        $password = $json->password;

        $repo = $this->getDoctrine()->getRepository(User::class);
        $user = $repo->findOneBy(['email' => $email]);
        if(empty($user)) {
            return $this->json(['verify' => 0, 'status' => false, 'user' => [],]);
        }

        $user = $this->formatToArray([$user]);
        $user = $user[0];

        $userPwdHash = $user['password'];
        $verify = password_verify($password, $userPwdHash);
        $status = true;
        if(!$verify) {
            $status = false;
            $user = [];
        }
        return $this->json(['verify' => $verify, 'status' => $status, 'user' => $user,]);
    }

    /**
     * @Route("/user/create", name="user_create")
     */
    public function create(Request $request): Response
    {
        $json = json_decode($request->getContent());
        $email = $json->email;
        $username = $json->username;
        $password   = password_hash(trim($json->password), PASSWORD_DEFAULT);

        $user = new User();

        $user->setEmail($email);
        $user->setUsername($username);
        $user->setPassword($password);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->json([
            'user_id' => $user->getId(),
        ]);
    }


    public function formatToArray($list): array
    {
        $data = [];
        foreach ($list as $item) {
            $data[] = [
                'id'    => $item->getId(),
                'username'   => $item->getUsername(),
                'email' => $item->getEmail(),
                'password'  => $item->getPassword(),
            ];
        }
        return $data;
    }

}
