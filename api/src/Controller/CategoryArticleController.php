<?php

namespace App\Controller;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryArticleController extends AbstractController
{
    /**
     * @Route("/category/article", name="category_article")
     */
    public function getList(): JsonResponse
    {
        $items = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $data = [];
        foreach ($items as $item) {
            $id = $item->getId();
            $data[$id] = [
                'id' => $id,
                'title' => $item->getTitle(),
            ];
        }
        return $this->json(['result' => $data]);
    }
}
