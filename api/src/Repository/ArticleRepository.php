<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry){
        parent::__construct($registry, Article::class);
    }

    public function getArticles($categoryId, $page, $limit){

        $sort = "asc";
        $page = ($page - 1) * $limit;

        $builder = $this
            ->createQueryBuilder('article')
            ->orderBy('article.id', $sort);

        if ($categoryId) {
            $builder->andWhere('article.category = :cat_id');
            $builder->setParameter('cat_id', $categoryId);
        }

        $results = [];
        $list = $builder
               ->setFirstResult($page)
               ->setMaxResults($limit);
                //->getQuery()->getResult();
        $paginator = new Paginator($list, true);

        foreach ($paginator as $item) {
            $results[] = $item;
        }

        return $results;
    }

    public function getArticlesFeedCount($categoryId)
    {
        $builder = $this->createQueryBuilder('article');

        if ($categoryId) {
            $builder->andWhere('article.category = :cat_id');
            $builder->setParameter('cat_id', $categoryId);
        }

        $totalCount = $builder->select('count(article.id) AS total')
                    ->getQuery()
                    ->getSingleScalarResult();
        return $totalCount;
    }

}
