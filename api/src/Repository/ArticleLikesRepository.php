<?php

namespace App\Repository;

use App\Entity\ArticleLikes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\User;

/**
 * @method ArticleLikes|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticleLikes|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticleLikes[]    findAll()
 * @method ArticleLikes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleLikesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArticleLikes::class);
    }

    public function getLikes($id, $type){
        $builder = $this
            ->createQueryBuilder('likes')
            ->orderBy('likes.id', "asc");

        if ($id) {
            $condition = 'likes.' .$type. ' = :id';
            $builder->andWhere($condition);
            $builder->setParameter('id', $id);
        }

        $results = $builder->getQuery()->getResult();

        return $results;
    }

    public function getLikeCompare($userId, $articleId){
        $builder = $this
            ->createQueryBuilder('likes')
            ->orderBy('likes.id', "asc");

        if ($userId) {
            $condition = 'likes.user_id = :user_id';
            $builder->andWhere($condition);
            $builder->setParameter('user_id', $userId);
        }

        if ($articleId) {
            $condition = 'likes.article_id = :article_id';
            $builder->andWhere($condition);
            $builder->setParameter('article_id', $articleId);
        }

        $results = $builder->getQuery()->getResult();

        return $results;
    }

    public function getArticleLikesCount($articleId)
    {
        $builder = $this->createQueryBuilder('likes');
        if ($articleId) {
            $builder->andWhere('likes.article_id = :article_id');
            $builder->setParameter('article_id', $articleId);
        }
        $totalCount = $builder->select('count(likes.id) AS total')
                               ->getQuery()->getSingleScalarResult();
        return $totalCount;
    }

    public function getActiveLikeUsers() {
        $qm = $this->createQueryBuilder('likes')
            ->select("likes.article_id, likes.user_id, us.username")
            ->leftJoin(User::class,"us","WITH", "likes.user_id = us.id");
        return $qm->getQuery()->getResult();
    }
}
