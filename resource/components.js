const ItemForm = {
    props: ['author'],
    data() {
        let author = this.author;
        return {
            itemForm: {
                id: 0,
                category_id: 0,
                title: '',
                content: '',
                author,
            }
        }
    },

    methods: {
        addArticle() {
            const url = '/article/create';
            this.post(url, this.itemForm, (response) => {
                alert('Статья сохранена');
                this.$emit('add_article', {status: 1});
            });
        },
    },

    created() {
        this.getCategories();
    },

    template: `
    <div class="mt-5 sm:mt-0" style="width:100%; margin: 4px 0px 5px 0px; z-index: 99999" >

            <div class="shadow overflow-hidden sm:rounded-md">
                <div class="px-2 py-3 bg-white sm:p-5">
                
                        <div style="display: flex;" >
                        
                            <div style="border:0px red solid; padding:5px; width: 50%" >
                                <label for="email-address" class="block text-sm font-medium text-gray-700">Заголовок</label>
                                <input type="text"  v-model="itemForm.title" style="height: 40px; border:1px gainsboro solid;"
                                       class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            </div>

                      
                            <div style="border:0px red solid; padding:5px; width: 50%" >
                                <label for="country" class="block text-sm font-medium text-gray-700">Категория</label>
                                <select  v-model="itemForm.category_id"
                                    class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                    <option v-for="(elem) in this.categories" :value="elem.id">{{elem.title}}</option>
                                </select>
                            </div>
                        
                        </div>

                        <div style="display: flex;">
                        
                            <div style="margin-top: 15px; width: 100%">
                                <label for="street-address" class="block text-sm font-medium text-gray-700">Статья</label>
                                <textarea  rows="4"  v-model="itemForm.content"
                                class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md" ></textarea>
                            </div>
      
                        </div>
                        
                        <div style="width:100%; margin: 10px 0px 0px 0px; padding-top: 25px; ">
                                <button @click="addArticle()"
                                        class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                   > Сохранить статью </button>
                        </div>
                        
                </div>

            </div>
    </div>`
}


const UserForm = {
    props: ['action_type'],
    data() {
        let modalTitle = (this.action_type == 'create') ? 'Регистрация' : 'Авторизация';
        return {
            modalTitle,
            userForm: {
                id: 0,
                email: '',
                username: '',
                password: '',
            }
        }
    },

    computed: {

        getActionType() {
            return this.action_type;
        },

        getModalTitle() {
            return this.modalTitle;
        },
    },

    methods: {
        sendUser() {
            let type = this.action_type;
            const url = '/user/' + type;
            this.post(url, this.userForm, (response) => {
                this.$emit('action_user', {type, response});
            });
        },

        modalClose() {
            this.$emit('modal_close', {type: 'close'});
        },
    },

    template: `

        <!-- Main modal -->
        <div id="authentication-modal" tabindex="-1" class="overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full justify-center items-center flex" aria-modal="true" role="dialog">
            <div class="relative p-4 w-full max-w-md h-full md:h-auto" style="top:0px">
                <!-- Modal content -->
                <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                    <button @click="modalClose" type="button" class="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white" data-modal-toggle="authentication-modal">
                        <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        <span class="sr-only">Close modal</span>
                    </button>
                    <div class="py-6 px-6 lg:px-8">
                        <h3 class="mb-4 text-xl font-medium text-gray-900 dark:text-white">{{modalTitle}}</h3>
                        <div class="space-y-6" >
                        
                            <div>
                                <label for="email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Email</label>
                                <input v-model="userForm.email" type="email" name="email" id="email" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="name@company.com" required="">
                            </div>
                            
                            <div>
                                <label for="password" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Пароль</label>
                                <input v-model="userForm.password" type="text" name="password" id="password" placeholder="Пароль" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required="">
                            </div>
                            
                            <div v-if="getActionType == 'create'" >
                                <label for="username" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Имя</label>
                                <input v-model="userForm.username" type="text" name="username" id="username" placeholder="Имя" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required="">
                            </div>

                            <button @click="sendUser()" type="submit" 
                                    class="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none  focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center  dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"> 
                                    Выполнить </button>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div> 

    `
}


const UserLikes = {

    created() {
        this.getUserLikes()
    },

    computed: {},

    template: `
      <div class="relative h-full flex flex-col justify-center space-y-8 sm:space-y-5 lg:space-y-8 xl:space-y-5 xl:px-5">
      
            <div class="sm:bg-white sm:rounded-lg sm:ring-1 sm:ring-slate-700/5 sm:shadow sm:p-3 lg:bg-transparent lg:rounded-none lg:ring-0 lg:shadow-none lg:p-0 xl:bg-white xl:rounded-lg xl:ring-1 xl:ring-slate-700/5 xl:shadow xl:p-3 dark:ring-white/10 dark:sm:bg-slate-900 dark:sm:ring-1 dark:lg:bg-transparent dark:lg:ring-0 dark:xl:bg-slate-900 dark:xl:ring-1"
                 style="opacity: 1;">
                 <h4 class="text-xs leading-5 font-mono pb-2 border-b border-slate-100 text-slate-500 dark:border-slate-200/10">
                     Активные пользователи</h4>

                <div v-for="(item, userId) in usersLikes" class="mt-2 sm:mt-3 lg:mt-2 xl:mt-3 text-slate-700 dark:text-slate-400 font-sans text-sm leading-6 
                            sm:text-base sm:leading-6 lg:text-sm lg:leading-6 xl:text-base xl:leading-6" style="border-bottom: 1px blue solid">
                     <div>{{item[0].username}}</div> 
                     <div style="font-size: 10px"> Количество лайков: {{item.length}}</div>
                </div>
            </div>
      </div>
    `
}