
const {createApp} = Vue;

const GlobalMixin = {
    data() {
        return {
            apiUrl : API_URL,
            categories: [],
            articles: [],
            categoryTitle: 'Все категории',
            categoryId: 0,
            article: {
                id: 0,
                title: '',
                author: '',
                content: '',
                category_id: '',
                published_at: '',
            },

            page: 1,
            limit: 5,
            totalCount: 0,
            formOpen: false,
            userFormOpen: false,
            userFormActionType: 'login',
            userName : 'Гость',
            userId: 0,
            paginatorList: [],
            usersLikes: [],
            usLikesPanelToggle: false,

            elemClass: 'block w-full pl-3.5 before:pointer-events-none before:absolute before:-left-1 before:top-1/2 before:h-1.5 before:w-1.5 before:-translate-y-1/2 before:rounded-full text-slate-500 before:hidden before:bg-slate-300 hover:text-slate-600 hover:before:block dark:text-slate-400 dark:before:bg-slate-700 dark:hover:text-slate-300',
            elemActiveClass: 'block w-full pl-3.5 before:pointer-events-none before:absolute before:-left-1 before:top-1/2 before:h-1.5 before:w-1.5 before:-translate-y-1/2 before:rounded-full font-semibold text-sky-500 before:bg-sky-500',
            inputClassWave: 'block  px-0 pb-0 pt-4 w-full text-sm text-gray-900 bg-gray-50 dark:bg-gray-700 border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer',
            labelClassWave: 'absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-3 z-10 origin-[0] left-2.5 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4',
            btnClassWave: 'hidden md:inline-flex items-center text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium  text-sm px-2 py-0 text-center ml-3',
        }
    },

    created() {
        //this.getCategories();
        //this.getArticles();
    },

    computed: {

        getCategoryActive() {
           return this.categoryId;
        },

        getTotalCount() {
            return this.totalCount;
        },

        getPaginatorList() {
           return this.paginatorList;
        },

        getUserFormActionType() {
            return this.userFormActionType;
        }
    },

    methods: {

        // loadPage(url = '#table-page', event = null) {
        //
        // },

        addLike(item) {
            let article_id = item.id;
            let user_id = this.userId;
            const url = `/article/like/create`;
            this.post(url, { article_id, user_id },  (response) => {
                alert(response.message);
                this.getArticles();
            });
        },

        userFormInit(type) {
            this.userFormActionType = type;
            this.userFormOpen = true;
        },

        userFormResponse(resp) {
            let action = resp.type;
            let response = resp.response;
            if(action == 'login') {
                this.saveAuthInfo(response)
            } else {
                this.userFormOpen = false;
            }
        },

        saveAuthInfo(response) {
            if(response.status) {
                this.userName = response.user.username;
                this.userId = response.user.id;
                this.userFormOpen = false;
                localStorage.setItem('user_id', this.userId)
                localStorage.setItem('user_name', this.userName)
            } else {
                alert('неправильный логин или пароль');
            }
        },

        userModalClose(resp) {
            this.userFormOpen = false;
        },

        deleteArticle(item) {
            const url = '/article/delete/' + item.id;
            this.get(url, (response) => {
                alert('Статья удалена');
                this.getArticles();
            });
        },

        loadPaginatorPage(pageNum) {
            this.page = pageNum;
            this.getArticles();
        },

        selectCategory(item, index){
            this.categoryId    = item.id;
            this.categoryTitle = item.title;
            this.page = 1;
            this.getArticles();
        },

        getCategories() {
            const url = '/category/article';
            this.get(url, (response) => {
                this.categories = response.result;
            });
        },

        getUserLikes() {
            const url = `/article/active-users/likes`;
            this.get(url, (response) => {
                this.usersLikes = response.result
            });
        },

        getArticles() {
            const catId = this.categoryId;
            const page  = this.page;
            const limit = this.limit;
            const url = `/article/list/${catId}/${page}/${limit}`;
            this.get(url, (response) => {
                this.articles = response.result;
                this.totalCount = response.total_count;
                this.setPaginator();
            });
        },

        addArticleResponse(){
            this.categoryId = 0;
            this.page = 1;
            this.formOpen = false;
            this.getArticles();
        },

        formOpenToggle() {
            this.formOpen = !this.formOpen;
        },

        get(url, callback) {
            const apiUrl = this.apiUrl + url;
            axios.get(apiUrl).then((response) => {
                callback(response.data)
            }).catch((error) => {
                console.log(error);
            })
        },

        post(url, postData, callback) {
            const apiUrl = this.apiUrl + url;
            axios.post(apiUrl, postData).then((response) => {
                callback(response.data)
            }).catch((error) => {
                console.log(error);
            });
        },

        setPaginator() {
            if(!this.totalCount) return false;
            this.paginatorList = [];
            let pageCount =  Math.ceil(this.totalCount / this.limit);
            for(let i = 0; i < pageCount; i++) {
                this.paginatorList.push(i + 1);
            }
        },

    }
}

const App = createApp({

    created() {

        this.getCategories();
        this.getArticles();

        this.userId = localStorage.getItem('user_id');
        this.userName = localStorage.getItem('user_name');
        if(!this.userId) this.userId = 0;
        if(!this.userName) this.userName = 'Гость';

    },

});

App.mixin(GlobalMixin);
App.component('item-form', ItemForm);
App.component('user-form', UserForm);
App.component('user-likes', UserLikes);
App.mount('#vue-app');
