# symfony5-rest-api-articles
Тестовый проект на symfony5 статьи 

###
Корневые папки
######
api    / backend symfony
######
index.php / frontend 
  - изменить в этом файле переменную API_URL (для связи фронта и бека)
  - const API_URL = 'http://symfony-site/test-articles/api/public';
######

использовано во фронте
  - vuejs (использовано в режиме библиотеки)
  - tailwindcss

##

REST API
######

api/public  точка входа в api symfony

### 

Для установки
######

в папке api/ выполнить 
  ######
  - composer install
  ######
  - php bin/console doctrine:database:create
  ######
  - php bin/console make:migration
  ######
  - php bin/console doctrine:migrations:migrate
  ######
  - в созданную базу импортировать symfony_test.sql


### 

Роуты

######
/article/list/{cat_id}/{page}/{limit}
######
/article/create 
######
/article/update/{id}
######
/article/delete/{id}
######
/article/get-one/{id}
######
/article/likes/all
######
/article/likes/{id}/{type} 
######
/article/active-users/likes
######
/article/like/create
######
/category/article
######
/user/list
######
/user/login
######
/user/create
######





